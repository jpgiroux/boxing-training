use lazy_static::lazy_static;
use std::collections::HashMap;

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub enum Locales {
    #[default]
    Fr,
    En,
}

#[derive(Debug, Clone)]
pub struct Translator {
    translations_fr: HashMap<&'static str, &'static str>,
    translations_en: HashMap<&'static str, &'static str>,
}

impl Translator {
    pub fn new() -> Translator {
        Translator {
            translations_fr: HashMap::new(),
            translations_en: HashMap::new(),
        }
    }

    pub fn add(&mut self, key: &'static str, fr: &'static str, en: &'static str) {
        self.translations_fr.insert(key, fr);
        self.translations_en.insert(key, en);
    }

    pub fn trans(&self, key: &str, locale: Locales) -> &str {
        let map = match locale {
            Locales::Fr => &self.translations_fr,
            Locales::En => &self.translations_en,
        };

        map.get(key).unwrap_or(&"!!!!")
    }
}

lazy_static! {
    pub static ref TRANSLATOR: Translator = {
        let mut t = Translator::new();
        t.add("reroll", "Regénérer les exercices", "Reroll exercices");
        t.add(
            "combo_laddering",
            "Échelle de combinaison",
            "Combination laddering",
        );
        t.add(
            "footwork_tango",
            "Jeu de pied tango",
            "Footwork Tango 2 step",
        );
        t.add("footwork_label", "Jeu de pied:", "Footwork :");
        t.add("Jazz!", "Jazz!", "Jazz!");
        t.add(
            "jazz_desc",
            "Lâche ton fou, improvise!",
            "Get funky, improvise!",
        );
        t.add("Jab", "Jab", "Jab");
        t.add("Cross", "Directe", "Cross");
        t.add("Lead hook", "Crochet avant", "Lead hook");
        t.add("Rear hook", "Crochet arrière", "Rear hook");
        t.add("Lead uppercut", "Uppercut avant", "Lead uppercut");
        t.add("Rear uppercut", "Uppercut arrière", "Rear uppercut");
        t.add("Jab body", "Jab au corp", "Jab body");
        t.add("Cross body", "Directe au corp", "Cross body");
        t.add("Lead hook body", "Crochet avant au corp", "Lead hook body");
        t.add(
            "Rear hook body",
            "Crochet arrière au corp",
            "Rear hook body",
        );
        t.add(
            "Lead uppercut body",
            "Uppercut avant au corp",
            "Lead uppercut body",
        );
        t.add(
            "Rear uppercut body",
            "Uppercut arrière au corp",
            "Rear uppercut body",
        );
        t.add("Slow Advance X3", "Avance lentement X3", "Slow Advance X3");
        t.add("Slow Retreat X3", "Retrait lentement X3", "Slow Retreat X3");
        t.add("Quick Advance", "Avance rapide", "Quick Advance");
        t.add(
            "Quick Retreat And Pivot Inside",
            "Retrait rapide et pivot intérieur",
            "Quick Retreat And Pivot Inside",
        );
        t.add(
            "Slow Outside Lateral X3",
            "Diagonal extérieur lentement X3",
            "Slow Outside Lateral X3",
        );
        t.add(
            "Slow Inside Lateral X3",
            "Diagonal intérieur lentement X3",
            "Slow Inside Lateral X3",
        );
        t.add(
            "Quick Outside Lateral And Pivot Outside",
            "Diagonal extérieur rapide et pivot extérieur",
            "Quick Outside Lateral And Pivot Outside",
        );
        t.add("Back And In", "Dehors et dedans", "Back And In");
        t.add("In And Out", "Dedans et dehors", "In And Out");
        t.add(
            "In And Out Pivot Outside",
            "Dedans et dehors et pivot extérieur",
            "In And Out Pivot Outside",
        );
        t.add(
            "In And Out Pivot Inside",
            "Dedans et dehors et pivot intérieur",
            "In And Out Pivot Inside",
        );
        t.add(
            "Shift Sidestep And Pivot",
            "Décalage latéral et pivot",
            "Shift Sidestep And Pivot",
        );
        t.add("Drop Shift X2", "Drop Shift X2", "Drop Shift X2");
        t.add(
            "Rear Shift And Pivot",
            "Rear Shift And Pivot",
            "Rear Shift And Pivot",
        );
        t.add("filter_heading", "Filtres", "Filters");
        t.add("jab_cross_punch_label", "Jab et Direct", "Jab and Cross");
        t.add("hooks_punch_label", "Crochets", "Hooks");
        t.add("uppercuts_punch_label", "Uppercuts", "Uppercuts");
        t.add("hands_selector_label", "Main", "Hand");
        t.add("hands_selector_lead_label", "Avant", "Lead");
        t.add("hands_selector_both_label", "Les deux", "Both");
        t.add("hands_selector_rear_label", "Arrière", "Rear");
        t
    };
}
