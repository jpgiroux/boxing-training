extern crate lazy_static;

mod combo_component;
mod exercise_component;
mod filter_component;
mod punch_component;
mod translator;

use crate::translator::Locales;
use boxing_training::Exercise;
use yew::prelude::*;

#[function_component(App)]
fn app() -> Html {
    let burger_state = use_state(|| false);

    let onclick = {
        let burger_state = burger_state.clone();
        Callback::from(move |_| burger_state.set(!(*burger_state)))
    };

    let locale_ctx = use_state(|| Locales::default());

    let fr_locale_click = {
        let locale_ctx = locale_ctx.clone();
        Callback::from(move |_| locale_ctx.set(Locales::Fr))
    };

    let en_locale_click = {
        let locale_ctx = locale_ctx.clone();
        Callback::from(move |_| locale_ctx.set(Locales::En))
    };


    let menu_class = if *burger_state {
        "navbar-burger is_yellow is-active"
    } else {
        "navbar-burger is_yellow"
    };
    let item_class = if *burger_state {
        "navbar-menu is-active is_blue"
    } else {
        "navbar-menu is_blue"
    };

    html! {
        <div class="is_blue">
            <ContextProvider<Locales> context={(*locale_ctx).clone()}>
                <nav class="navbar is_blue" role="navigation" aria-label="main navigation">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="#">
                            <h1 class="title is-1 has-text-light">{ "Boxing Training" }</h1>
                        </a>

                        <a role="button" class={menu_class} onclick={onclick} aria-label="menu" aria-expanded="false" data-target="nav">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>

                    <div id="nav" class={item_class}>
                        <div class="navbar-end">
                            <div class="navbar-item">
                                <a onclick={fr_locale_click}  class="is_yellow">{"Fr"}</a>
                            </div>
                            <div class="navbar-item">
                                <a onclick={en_locale_click} class="is_yellow">{"En"}</a>
                            </div>
                        </div>
                    </div>
                </nav>

                <section class="section">
                    <div class="container">
                        <div class="main">

                            <filter_component::FilterComponent/>


                           /* <button class="button is_yellow" >{ TRANSLATOR.trans("reroll", *locale_ctx) }</button> TODO: reimplement this*/
                        </div>
                    </div>
                </section>
            </ContextProvider<Locales>>
        </div>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::Renderer::<App>::new().render();
}
