use crate::punch_component;
use boxing_training::Combo;
use yew::{function_component, html, Html, Properties};

#[derive(Properties, Clone, PartialEq)]
pub struct ComboProps {
    pub combo: Combo,
}

#[function_component(ComboComponent)]
pub fn combo_component(props: &ComboProps) -> Html {
    match (props.combo.p3, props.combo.p4) {
        (Some(p3), Some(p4)) => {
            html! {
                <div class="columns">
                    <div class="column">
                        <punch_component::PunchComponent punch={props.combo.p1.clone()} />
                    </div>
                    <div class="column">
                        <punch_component::PunchComponent punch={props.combo.p2.clone()}/>
                    </div>
                    <div class="column">
                        <punch_component::PunchComponent punch={p3.clone()} />
                    </div>
                    <div class="column">
                        <punch_component::PunchComponent punch={p4.clone()} />
                    </div>
                </div>
            }
        }
        (Some(p3), _) => {
            html! {
                <div class="columns">
                    <div class="column">
                        <punch_component::PunchComponent punch={props.combo.p1.clone()} />
                    </div>
                    <div class="column">
                        <punch_component::PunchComponent punch={props.combo.p2.clone()} />
                    </div>
                    <div class="column">
                        <punch_component::PunchComponent punch={p3.clone()} />
                    </div>
                </div>
            }
        }
        _ => {
            html! {
                <div class="columns">
                    <div class="column">
                        <punch_component::PunchComponent punch={props.combo.p1.clone()} />
                    </div>
                    <div class="column">
                        <punch_component::PunchComponent punch={props.combo.p2.clone()}/>
                    </div>
                </div>
            }
        }
    }
}
