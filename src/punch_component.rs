use crate::translator::{Locales, TRANSLATOR};
use boxing_training::Punch;
use yew::{function_component, html, use_context, Html, Properties};

#[derive(Properties, Clone, PartialEq)]
pub struct PunchProps {
    pub punch: Punch,
}

#[function_component(PunchComponent)]
pub fn punch_component(props: &PunchProps) -> Html {
    let locale_ctx = use_context::<Locales>().unwrap_or_default();

    html! {
        <p class="punch">{TRANSLATOR.trans(&props.punch.to_string(), locale_ctx)}</p>
    }
}
