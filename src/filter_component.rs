
use crate::translator::{Locales, TRANSLATOR};
use crate::exercise_component;
use boxing_training::{CombinationFilters, PunchType, Hand, Target, Exercise};
use yew::{function_component, html, use_context, use_state, Callback, Event, Html, TargetCast };
use web_sys::HtmlInputElement;

#[derive(Debug, Clone, PartialEq)]
struct FilterComponentState {
    filters: CombinationFilters,
    exercises: [Exercise;3] 
}

impl FilterComponentState {

    fn new() -> Self {
        let mut filters = CombinationFilters::new();
        filters.select_punch(PunchType::JabNCross);
        filters.select_punch(PunchType::Hooks);
        filters.select_punch(PunchType::Uppercuts);
        filters.select_hand(Hand::Lead);
        filters.select_hand(Hand::Rear);
        filters.select_target(Target::Head);
        filters.select_target(Target::Body);
        
        let exercises = [
            Exercise::generate_rng(false, &filters),
            Exercise::generate_rng(false, &filters),
            Exercise::generate_rng(true, &filters),
        ];

        FilterComponentState{ filters, exercises }
    }

    fn reroll_exercises(&mut self) {
        self.exercises = [
            Exercise::generate_rng(false, &self.filters),
            Exercise::generate_rng(false, &self.filters),
            Exercise::generate_rng(true, &self.filters),
        ];
    }
}

#[function_component(FilterComponent)]
pub fn filter_component() -> Html {
    let locale_ctx = use_context::<Locales>().unwrap_or_default();

    let is_collapsed_ctx = use_state(|| false);

    let collapsible_click = {
        let collapsible_ctx = is_collapsed_ctx.clone();
        Callback::from(move |_| collapsible_ctx.set(!(*collapsible_ctx)))
    };


    // 0 for the content display style
    // 1 for the icon css class
    // 2 for the aria-hidden state
    let collapsible_infos = match *is_collapsed_ctx {
        true => { ("display: block", "fas fa-angle-up", "false")},
        false => { ("display: none", "fas fa-angle-down", "true")}
    };

    
    let ctx = use_state(|| {
        FilterComponentState::new()
    });

    let jab_cross_change = {
        let ctx = ctx.clone();
        Callback::from(move |_| {
            ctx.set({
                let mut new_ctx = (*ctx).clone(); 
                new_ctx.filters.toggle_punch(PunchType::JabNCross);
                new_ctx.reroll_exercises();
                new_ctx
        })
        })
    };

    let hooks_change = {
        let ctx = ctx.clone();
        Callback::from(move |_| {
            ctx.set({
                let mut new_ctx = (*ctx).clone(); 
                new_ctx.filters.toggle_punch(PunchType::Hooks);
                new_ctx.reroll_exercises();
                new_ctx
        })
        })
    };

    let uppercuts_change = {
        let ctx = ctx.clone();
        Callback::from(move |_| {
            ctx.set({
                let mut new_ctx = (*ctx).clone(); 
                new_ctx.filters.toggle_punch(PunchType::Uppercuts);
                new_ctx.reroll_exercises();
                new_ctx
        })
        })
    };

    let slider_hand_change = {
        let ctx = ctx.clone();
        Callback::from(move |e : Event| {
            let input = e.target_dyn_into::<HtmlInputElement>();
            let value = input.unwrap().value().parse::<i32>().unwrap_or(1);
            ctx.set({
                let mut new_ctx = (*ctx).clone();
                new_ctx.filters.select_hand_from_i32(value);
                new_ctx.reroll_exercises();
                new_ctx
            })
        })
    };

    

    log::info!("{:#?}", *ctx);

    html! {
       <>
       <div class="block">
           <div class="card">
               <header class="card-header">
                   <p class="card-header-title">
                       { TRANSLATOR.trans("filter_heading", locale_ctx)}
                   </p>
                   <button class="card-header-icon" aria-label="more options" onclick={collapsible_click}>
                     <span class="icon">
                       <i class={collapsible_infos.1} aria-hidden={collapsible_infos.2}></i>
                     </span>
                   </button>
               </header>
               <div class="card-content" style={collapsible_infos.0}>
                   <div class="content">
                       <fieldset style="display:flex; justify-content: space-evenly;">
                            <legend>{"Punch types"}</legend>
                           <label class="checkbox" for="checkJabCrossPunchEnable">
                               <input type="checkbox" name="checkJabCrossPunchEnable" id="checkJabCrossPunchEnable" style="margin-right:0.5em;"
                                    checked={ctx.filters.is_punch_selected(PunchType::JabNCross)} onclick={jab_cross_change}/>
                               { TRANSLATOR.trans("jab_cross_punch_label", locale_ctx) }
                           </label>
                           <label class="checkbox" for="checkHooksPunchEnable">
                               <input type="checkbox" name="checkHooksPunchEnable" id="checkHooksPunchEnable" style="margin-right:0.5em;"
                                    checked={ctx.filters.is_punch_selected(PunchType::Hooks)} onclick={hooks_change} />
                               { TRANSLATOR.trans("hooks_punch_label", locale_ctx) }
                           </label>
                           <label class="checkbox" for="checkUppercutsPunchEnable">
                               <input type="checkbox" name="checkUppercutsPunchEnable" id="checkUppercutsPunchEnable" style="margin-right:0.5em;"
                                    checked={ctx.filters.is_punch_selected(PunchType::Uppercuts)} onclick={uppercuts_change}/>
                               { TRANSLATOR.trans("uppercuts_punch_label", locale_ctx) }
                           </label>
                       </fieldset>
                       <div style="margin-top: 10px">
                            <label for="volume">{ TRANSLATOR.trans("hands_selector_label", locale_ctx) }</label>
                            <div style="margin: 10px 20px 20px 20px;">
                                <div style="display: flex; justify-content: space-between;">
                                    <span>{ TRANSLATOR.trans("hands_selector_lead_label", locale_ctx) }</span>
                                    <span>{ TRANSLATOR.trans("hands_selector_both_label", locale_ctx) }</span>
                                    <span>{ TRANSLATOR.trans("hands_selector_rear_label", locale_ctx) }</span>
                                </div>
                                <input type="range" id="handSelector" name="handSelector" min="0" max="2" style="width: 100%; accent-color: white;" value={ctx.filters.get_hand_as_i32().to_string()} onchange={slider_hand_change}/>
                            </div>
                       </div>
                      
                       //TODO add target filters
                   </div>
               </div>
           </div>
       </div>
       <exercise_component::ExerciseComponent exercise={ctx.exercises[0]} />
       <exercise_component::ExerciseComponent exercise={ctx.exercises[1]} />
       <exercise_component::ExerciseComponent exercise={ctx.exercises[2]} />
       </>
    }
}
