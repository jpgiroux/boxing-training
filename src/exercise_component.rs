use super::Exercise;
use crate::combo_component;
use crate::translator::{Locales, TRANSLATOR};
use yew::{function_component, html, use_context, Html, Properties};

#[derive(Properties, Clone, PartialEq)]
pub struct ExerciseProps {
    pub exercise: Exercise,
}

#[function_component(ExerciseComponent)]
pub fn exercise_component(props: &ExerciseProps) -> Html {
    let locale_ctx = use_context::<Locales>().unwrap_or_default();

    match props.exercise {
        Exercise::CombinationLaddering(c) => {
            html! {
                <div class="block">
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">
                                { TRANSLATOR.trans("combo_laddering", locale_ctx)}
                            </p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <combo_component::ComboComponent combo={c.clone()} />
                            </div>
                        </div>
                    </div>
                </div>
            }
        }
        Exercise::FootworkTangoTwoStep(c, f) => {
            html! {
                <div class="block">
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">
                                {TRANSLATOR.trans("footwork_tango", locale_ctx)}
                            </p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div class="footwork">
                                    {format!("{} {}", TRANSLATOR.trans("footwork_label", locale_ctx), TRANSLATOR.trans(&f.to_string(), locale_ctx)) }
                                </div>
                                <div>
                                    <combo_component::ComboComponent combo={c.clone()} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        }
        Exercise::Jazz => {
            html! {
                <div class="block">
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">
                                {TRANSLATOR.trans("Jazz!", locale_ctx)}
                            </p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div>
                                    {TRANSLATOR.trans("jazz_desc", locale_ctx)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        }
    }
}
