use rand::Rng;
use std::collections::HashSet;
use std::fmt;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Hand {
    Lead,
    Rear,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum PunchType {
    JabNCross,
    Hooks,
    Uppercuts,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Target {
    Head,
    Body,
}

/*********************************
 * Punch
 ********************************/
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Punch {
    hand: Hand,
    ptype: PunchType,
    target: Target,
}

impl Punch {
    pub fn jab() -> Self {
        Punch {
            hand: Hand::Lead,
            ptype: PunchType::JabNCross,
            target: Target::Head,
        }
    }

    pub fn cross() -> Self {
        Punch {
            hand: Hand::Rear,
            ptype: PunchType::JabNCross,
            target: Target::Head,
        }
    }

    pub fn lead_hook() -> Self {
        Punch {
            hand: Hand::Lead,
            ptype: PunchType::Hooks,
            target: Target::Head,
        }
    }

    pub fn rear_hook() -> Self {
        Punch {
            hand: Hand::Rear,
            ptype: PunchType::Hooks,
            target: Target::Head,
        }
    }

    pub fn lead_uppercut() -> Self {
        Punch {
            hand: Hand::Lead,
            ptype: PunchType::Uppercuts,
            target: Target::Head,
        }
    }

    pub fn rear_uppercut() -> Self {
        Punch {
            hand: Hand::Rear,
            ptype: PunchType::Uppercuts,
            target: Target::Head,
        }
    }

    pub fn jab_body() -> Self {
        Punch {
            hand: Hand::Lead,
            ptype: PunchType::JabNCross,
            target: Target::Body,
        }
    }

    pub fn cross_body() -> Self {
        Punch {
            hand: Hand::Rear,
            ptype: PunchType::JabNCross,
            target: Target::Body,
        }
    }

    pub fn lead_hook_body() -> Self {
        Punch {
            hand: Hand::Lead,
            ptype: PunchType::Hooks,
            target: Target::Body,
        }
    }

    pub fn rear_hook_body() -> Self {
        Punch {
            hand: Hand::Rear,
            ptype: PunchType::Hooks,
            target: Target::Body,
        }
    }

    pub fn lead_uppercut_body() -> Self {
        Punch {
            hand: Hand::Lead,
            ptype: PunchType::Uppercuts,
            target: Target::Body,
        }
    }

    pub fn rear_uppercut_body() -> Self {
        Punch {
            hand: Hand::Rear,
            ptype: PunchType::Uppercuts,
            target: Target::Body,
        }
    }
}

impl fmt::Display for Punch {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let hand_specifier = match (self.hand, self.ptype) {
            (_, PunchType::JabNCross) => "",
            (Hand::Lead, _) => "Lead",
            (Hand::Rear, _) => "Rear",
        };

        let punch_specifier = match (self.hand, self.ptype) {
            (Hand::Lead, PunchType::JabNCross) => "Jab",
            (Hand::Rear, PunchType::JabNCross) => "Cross",
            (_, PunchType::Hooks) => " hook",
            (_, PunchType::Uppercuts) => " uppercut",
        };

        let body_specifier = match self.target {
            Target::Body => " body",
            _ => "",
        };

        write!(f, "{}{}{}", hand_specifier, punch_specifier, body_specifier)
    }
}

/*********************************
 * Foot Work
 ********************************/
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum FootWork {
    SlowAdvanceX3,
    SlowRetreatX3,
    QuickAdvance,
    QuickRetreatAndPivotInside,
    SlowOutsideLateralX3,
    SlowInsideLateralX3,
    QuickOutsideLateralAndPivotOutside,
    BackAndIn,
    InAndOut,
    InAndOutPivotOutside,
    InAndOutPivotInside,
    ShiftSidestepAndPivot,
    DropShiftX2,
    RearShiftAndPivot,
}

impl fmt::Display for FootWork {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match &self {
            FootWork::SlowAdvanceX3 => "Slow Advance X3",
            FootWork::SlowRetreatX3 => "Slow Retreat X3",
            FootWork::QuickAdvance => "Quick Advance",
            FootWork::QuickRetreatAndPivotInside => "Quick Retreat And Pivot Inside",
            FootWork::SlowOutsideLateralX3 => "Slow Outside Lateral X3",
            FootWork::SlowInsideLateralX3 => "Slow Inside Lateral X3",
            FootWork::QuickOutsideLateralAndPivotOutside => {
                "Quick Outside Lateral And Pivot Outside"
            }
            FootWork::BackAndIn => "Back And In",
            FootWork::InAndOut => "In And Out",
            FootWork::InAndOutPivotOutside => "In And Out Pivot Outside",
            FootWork::InAndOutPivotInside => "In And Out Pivot Inside",
            FootWork::ShiftSidestepAndPivot => "Shift Sidestep And Pivot",
            FootWork::DropShiftX2 => "Drop Shift X2",
            FootWork::RearShiftAndPivot => "Rear Shift And Pivot",
        };
        write!(f, "{}", s)
    }
}

#[derive(Debug)]
pub struct FootWorks {
    one_step: [FootWork; 1],
    two_step: [FootWork; 9],
    three_step: [FootWork; 4],
}

impl FootWorks {
    pub fn new() -> FootWorks {
        FootWorks {
            one_step: [FootWork::QuickAdvance],
            two_step: [
                FootWork::QuickRetreatAndPivotInside,
                FootWork::QuickOutsideLateralAndPivotOutside,
                FootWork::BackAndIn,
                FootWork::InAndOut,
                FootWork::InAndOutPivotOutside,
                FootWork::InAndOutPivotInside,
                FootWork::ShiftSidestepAndPivot,
                FootWork::DropShiftX2,
                FootWork::RearShiftAndPivot,
            ],
            three_step: [
                FootWork::SlowAdvanceX3,
                FootWork::SlowRetreatX3,
                FootWork::SlowInsideLateralX3,
                FootWork::SlowOutsideLateralX3,
            ],
        }
    }

    pub fn get_two_step_random(&self) -> FootWork {
        let chosen = rand::thread_rng().gen_range(0..self.two_step.len());
        self.two_step[chosen]
    }
}

/*********************************
 * Combo
 ********************************/
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Combo {
    pub p1: Punch,
    pub p2: Punch,
    pub p3: Option<Punch>,
    pub p4: Option<Punch>,
}

impl Combo {
    pub fn new_two(p1: Punch, p2: Punch) -> Combo {
        Combo {
            p1,
            p2,
            p3: None,
            p4: None,
        }
    }

    pub fn new_three(p1: Punch, p2: Punch, p3: Punch) -> Combo {
        Combo {
            p1,
            p2,
            p3: Some(p3),
            p4: None,
        }
    }

    pub fn new_four(p1: Punch, p2: Punch, p3: Punch, p4: Punch) -> Combo {
        Combo {
            p1,
            p2,
            p3: Some(p3),
            p4: Some(p4),
        }
    }
}

impl fmt::Display for Combo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.p3 {
            Some(p3) => match self.p4 {
                Some(p4) => {
                    write!(f, "{} => {} => {} => {}", self.p1, self.p2, p3, p4)
                }
                None => {
                    write!(f, "{} => {} => {}", self.p1, self.p2, p3)
                }
            },
            None => {
                write!(f, "{} => {} ", self.p1, self.p2)
            }
        }
    }
}

impl Combo {
    fn satisfies(&self, combo_filters: &CombinationFilters) -> bool {
        let punch_type_satisfied = combo_filters.punch_types_selected.contains(&self.p1.ptype)
            && combo_filters.punch_types_selected.contains(&self.p2.ptype)
            && (self.p3.is_none()
                || self
                    .p3
                    .is_some_and(|p3| combo_filters.punch_types_selected.contains(&p3.ptype)))
            && (self.p4.is_none()
                || self
                    .p4
                    .is_some_and(|p4| combo_filters.punch_types_selected.contains(&p4.ptype)));

        let hand_satisfied = combo_filters.hands_selected.contains(&self.p1.hand)
            && combo_filters.hands_selected.contains(&self.p2.hand)
            && (self.p3.is_none()
                || self
                    .p3
                    .is_some_and(|p3| combo_filters.hands_selected.contains(&p3.hand)))
            && (self.p4.is_none()
                || self
                    .p4
                    .is_some_and(|p4| combo_filters.hands_selected.contains(&p4.hand)));

        let target_satisfied = combo_filters.target_selected.contains(&self.p1.target)
            && combo_filters.target_selected.contains(&self.p2.target)
            && (self.p3.is_none()
                || self
                    .p3
                    .is_some_and(|p3| combo_filters.target_selected.contains(&p3.target)))
            && (self.p4.is_none()
                || self
                    .p4
                    .is_some_and(|p4| combo_filters.target_selected.contains(&p4.target)));

        punch_type_satisfied && hand_satisfied && target_satisfied
    }
}

/***************************************
 * Combination Menu
 **************************************/
pub struct CombinationMenu {
    combos: [Combo; 61],
}

impl CombinationMenu {
    pub fn new() -> CombinationMenu {
        CombinationMenu {
            combos: [
                Combo::new_two(Punch::jab(), Punch::jab()),
                Combo::new_two(Punch::jab(), Punch::jab_body()),
                Combo::new_two(Punch::jab(), Punch::cross()),
                Combo::new_two(Punch::jab(), Punch::cross_body()),
                Combo::new_two(Punch::jab(), Punch::lead_hook()),
                Combo::new_two(Punch::jab(), Punch::lead_uppercut()),
                Combo::new_two(Punch::jab(), Punch::rear_uppercut()),
                Combo::new_two(Punch::jab_body(), Punch::jab()),
                Combo::new_two(Punch::jab_body(), Punch::jab_body()),
                Combo::new_two(Punch::jab_body(), Punch::cross()),
                Combo::new_two(Punch::jab_body(), Punch::cross_body()),
                Combo::new_two(Punch::jab_body(), Punch::lead_hook()),
                Combo::new_two(Punch::jab_body(), Punch::rear_hook()),
                Combo::new_two(Punch::cross(), Punch::lead_hook()),
                Combo::new_two(Punch::cross(), Punch::lead_hook_body()),
                Combo::new_two(Punch::cross_body(), Punch::lead_hook()),
                Combo::new_two(Punch::lead_hook(), Punch::cross_body()),
                Combo::new_two(Punch::lead_hook(), Punch::lead_hook()),
                Combo::new_two(Punch::lead_hook(), Punch::rear_hook()),
                Combo::new_two(Punch::lead_hook(), Punch::lead_uppercut()),
                Combo::new_two(Punch::lead_hook(), Punch::rear_uppercut()),
                Combo::new_two(Punch::rear_hook(), Punch::rear_hook()),
                Combo::new_two(Punch::rear_hook(), Punch::rear_uppercut()),
                Combo::new_two(Punch::lead_uppercut(), Punch::jab()),
                Combo::new_two(Punch::lead_uppercut(), Punch::rear_hook()),
                Combo::new_two(Punch::lead_uppercut(), Punch::rear_uppercut()),
                Combo::new_two(Punch::rear_uppercut(), Punch::jab()),
                Combo::new_two(Punch::rear_uppercut(), Punch::cross()),
                Combo::new_two(Punch::rear_uppercut(), Punch::lead_hook()),
                Combo::new_two(Punch::rear_uppercut(), Punch::rear_hook()),
                Combo::new_three(Punch::jab(), Punch::jab(), Punch::jab()),
                Combo::new_three(Punch::jab(), Punch::jab(), Punch::cross()),
                Combo::new_three(Punch::jab(), Punch::jab_body(), Punch::lead_hook()),
                Combo::new_three(Punch::jab(), Punch::cross(), Punch::jab()),
                Combo::new_three(Punch::jab(), Punch::cross(), Punch::lead_hook()),
                Combo::new_three(Punch::jab(), Punch::cross(), Punch::lead_uppercut()),
                Combo::new_three(Punch::jab(), Punch::cross_body(), Punch::lead_hook()),
                Combo::new_three(Punch::jab(), Punch::lead_hook(), Punch::cross()),
                Combo::new_three(Punch::jab(), Punch::lead_hook(), Punch::rear_hook()),
                Combo::new_three(Punch::jab(), Punch::lead_hook(), Punch::lead_uppercut()),
                Combo::new_three(Punch::jab(), Punch::lead_hook(), Punch::rear_uppercut()),
                Combo::new_three(Punch::jab(), Punch::lead_hook_body(), Punch::lead_hook()),
                Combo::new_three(Punch::jab(), Punch::lead_uppercut(), Punch::lead_hook()),
                Combo::new_three(Punch::jab(), Punch::lead_uppercut(), Punch::rear_hook()),
                Combo::new_three(Punch::jab(), Punch::rear_uppercut(), Punch::lead_hook()),
                Combo::new_three(Punch::cross(), Punch::lead_hook(), Punch::cross()),
                Combo::new_three(Punch::lead_hook(), Punch::cross(), Punch::lead_hook()),
                Combo::new_three(Punch::lead_hook(), Punch::lead_uppercut(), Punch::cross()),
                Combo::new_three(
                    Punch::lead_hook(),
                    Punch::lead_uppercut(),
                    Punch::rear_hook(),
                ),
                Combo::new_three(
                    Punch::lead_hook_body(),
                    Punch::lead_hook(),
                    Punch::rear_uppercut(),
                ),
                Combo::new_three(
                    Punch::lead_uppercut(),
                    Punch::rear_hook(),
                    Punch::lead_hook(),
                ),
                Combo::new_four(
                    Punch::jab(),
                    Punch::cross(),
                    Punch::lead_hook_body(),
                    Punch::rear_uppercut(),
                ),
                Combo::new_four(
                    Punch::jab(),
                    Punch::cross_body(),
                    Punch::lead_hook(),
                    Punch::rear_hook_body(),
                ),
                Combo::new_four(
                    Punch::jab(),
                    Punch::lead_hook(),
                    Punch::cross(),
                    Punch::lead_hook(),
                ),
                Combo::new_four(
                    Punch::jab(),
                    Punch::lead_hook_body(),
                    Punch::lead_hook(),
                    Punch::rear_uppercut(),
                ),
                Combo::new_four(
                    Punch::cross(),
                    Punch::lead_hook(),
                    Punch::cross(),
                    Punch::lead_hook_body(),
                ),
                Combo::new_four(
                    Punch::cross(),
                    Punch::lead_hook(),
                    Punch::cross_body(),
                    Punch::lead_uppercut(),
                ),
                Combo::new_four(
                    Punch::lead_hook(),
                    Punch::rear_hook(),
                    Punch::lead_uppercut(),
                    Punch::rear_uppercut(),
                ),
                Combo::new_four(
                    Punch::lead_hook(),
                    Punch::lead_uppercut(),
                    Punch::rear_hook(),
                    Punch::rear_uppercut(),
                ),
                Combo::new_four(
                    Punch::lead_hook(),
                    Punch::rear_uppercut(),
                    Punch::lead_uppercut(),
                    Punch::rear_hook(),
                ),
                Combo::new_four(
                    Punch::lead_uppercut(),
                    Punch::lead_hook(),
                    Punch::rear_uppercut(),
                    Punch::rear_hook(),
                ),
            ],
        }
    }

    pub fn get_combos(&self) -> &[Combo] {
        &self.combos
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct CombinationFilters {
    punch_types_selected: HashSet<PunchType>,
    hands_selected: HashSet<Hand>,
    target_selected: HashSet<Target>,
}

impl CombinationFilters {
    pub fn new() -> Self {
        CombinationFilters {
            punch_types_selected: HashSet::new(),
            hands_selected: HashSet::new(),
            target_selected: HashSet::new(),
        }
    }

    pub fn select_punch(&mut self, punch_type: PunchType) {
        self.punch_types_selected.insert(punch_type);
    }

    pub fn is_punch_selected(&self, punch_type: PunchType) -> bool {
        self.punch_types_selected.contains(&punch_type)
    }

	pub fn toggle_punch(&mut self, punch_type: PunchType) {
		if self.punch_types_selected.contains(&punch_type) {
			self.punch_types_selected.remove(&punch_type);
		} else {
			self.punch_types_selected.insert(punch_type);
		}
	}

    pub fn select_hand(&mut self, hand: Hand) {
        self.hands_selected.insert(hand);
    }

    pub fn remove_hand(&mut self, hand: Hand) {
        self.hands_selected.remove(&hand);
    }

    pub fn select_hand_from_i32(&mut self, i : i32 ) {
        self.hands_selected.clear();
        if i <= 1 {
            self.hands_selected.insert(Hand::Lead);
        }

        if i >= 1 {
            self.hands_selected.insert(Hand::Rear);
        }
    }

    pub fn get_hand_as_i32(&self) -> i32 {
        if self.hands_selected.contains(&Hand::Lead) && self.hands_selected.contains(&Hand::Rear) {
            1
        }
        else if self.hands_selected.contains(&Hand::Lead) {
            0
        } else {
            2
        }
    }

    pub fn select_target(&mut self, target: Target) {
        self.target_selected.insert(target);
    }

    pub fn filter(&self, menu: &CombinationMenu) -> Vec<Combo> {
        menu.get_combos()
            .iter()
            .filter(|c| c.satisfies(&self))
            .copied()
            .collect()
    }
}

/************************************
 * Exercise
 ***********************************/
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Exercise {
    CombinationLaddering(Combo),
    FootworkTangoTwoStep(Combo, FootWork),
    Jazz,
}

impl Exercise {
    pub fn generate_rng(with_jazz: bool, filters: &CombinationFilters) -> Exercise {
        let upper_range = match with_jazz {
            true => 3,
            _ => 2,
        };

        let chosen = rand::thread_rng().gen_range(0..upper_range);
        let cm = CombinationMenu::new();
        let fws = FootWorks::new();

        let filtered_combos = filters.filter(&cm);

        // we filtered too aggressively
        // todo handle this so the user know about why he get just jazz
        if filtered_combos.is_empty() {
            return Exercise::Jazz;
        }

        let chosen_combo = filtered_combos[rand::thread_rng().gen_range(0..filtered_combos.len())];

        match chosen {
            0 => Exercise::CombinationLaddering(chosen_combo),
            1 => Exercise::FootworkTangoTwoStep(chosen_combo, fws.get_two_step_random()),
            _ => Exercise::Jazz,
        }
    }
}

impl fmt::Display for Exercise {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Exercise::CombinationLaddering(c) => write!(f, "Combination Laddering with\n  {}", c),
            Exercise::FootworkTangoTwoStep(c, fw) => write!(
                f,
                "Footwork Tango 2-Step with\n  {}\n and Footwork:\n  {}",
                c, fw
            ),
            Exercise::Jazz => write!(f, "Jazz"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn given_combo_with_everything_and_filter_all_selected_when_checking_if_combo_satifies_filter_then_true(
    ) {
        let combo = Combo::new_four(
            Punch::jab(),
            Punch::cross_body(),
            Punch::lead_hook(),
            Punch::rear_uppercut(),
        );
        let mut filter = CombinationFilters::new();
        filter.select_punch(PunchType::JabNCross);
        filter.select_punch(PunchType::Hooks);
        filter.select_punch(PunchType::Uppercuts);
        filter.select_hand(Hand::Lead);
        filter.select_hand(Hand::Rear);
        filter.select_target(Target::Head);
        filter.select_target(Target::Body);

        assert_eq!(true, combo.satisfies(&filter));
    }

    #[test]
    fn given_combo_with_everything_and_filter_missing_punchtype_when_checking_if_combo_satisfies_filter_then_false(
    ) {
        let combo = Combo::new_four(
            Punch::jab(),
            Punch::cross_body(),
            Punch::lead_hook(),
            Punch::rear_uppercut(),
        );

        let mut filter_no_jab = CombinationFilters::new();
        filter_no_jab.select_punch(PunchType::Hooks);
        filter_no_jab.select_punch(PunchType::Uppercuts);
        filter_no_jab.select_hand(Hand::Lead);
        filter_no_jab.select_hand(Hand::Rear);
        filter_no_jab.select_target(Target::Head);
        filter_no_jab.select_target(Target::Body);

        let mut filter_no_hooks = CombinationFilters::new();
        filter_no_hooks.select_punch(PunchType::JabNCross);
        filter_no_hooks.select_punch(PunchType::Uppercuts);
        filter_no_hooks.select_hand(Hand::Lead);
        filter_no_hooks.select_hand(Hand::Rear);
        filter_no_hooks.select_target(Target::Head);
        filter_no_hooks.select_target(Target::Body);

        let mut filter_no_uppercuts = CombinationFilters::new();
        filter_no_uppercuts.select_punch(PunchType::JabNCross);
        filter_no_uppercuts.select_punch(PunchType::Hooks);
        filter_no_uppercuts.select_hand(Hand::Lead);
        filter_no_uppercuts.select_hand(Hand::Rear);
        filter_no_uppercuts.select_target(Target::Head);
        filter_no_uppercuts.select_target(Target::Body);

        assert_eq!(false, combo.satisfies(&filter_no_jab));
        assert_eq!(false, combo.satisfies(&filter_no_hooks));
        assert_eq!(false, combo.satisfies(&filter_no_uppercuts));
    }

    #[test]
    fn given_combo_with_everything_and_filter_missing_hand_when_checking_if_combo_satisfies_filter_then_false(
    ) {
        let combo = Combo::new_four(
            Punch::jab(),
            Punch::cross_body(),
            Punch::lead_hook(),
            Punch::rear_uppercut(),
        );

        let mut filter_no_lead = CombinationFilters::new();
        filter_no_lead.select_punch(PunchType::JabNCross);
        filter_no_lead.select_punch(PunchType::Hooks);
        filter_no_lead.select_punch(PunchType::Uppercuts);
        filter_no_lead.select_hand(Hand::Rear);
        filter_no_lead.select_target(Target::Head);
        filter_no_lead.select_target(Target::Body);

        let mut filter_no_rear = CombinationFilters::new();
        filter_no_rear.select_punch(PunchType::JabNCross);
        filter_no_rear.select_punch(PunchType::Hooks);
        filter_no_rear.select_punch(PunchType::Uppercuts);
        filter_no_rear.select_hand(Hand::Lead);
        filter_no_rear.select_target(Target::Head);
        filter_no_rear.select_target(Target::Body);

        assert_eq!(false, combo.satisfies(&filter_no_lead));
        assert_eq!(false, combo.satisfies(&filter_no_rear));
    }

    #[test]
    fn given_combo_with_everything_and_filter_missing_target_when_checking_if_combo_satisfies_filter_then_false(
    ) {
        let combo = Combo::new_four(
            Punch::jab(),
            Punch::cross_body(),
            Punch::lead_hook(),
            Punch::rear_uppercut(),
        );

        let mut filter_no_head = CombinationFilters::new();
        filter_no_head.select_punch(PunchType::JabNCross);
        filter_no_head.select_punch(PunchType::Hooks);
        filter_no_head.select_punch(PunchType::Uppercuts);
        filter_no_head.select_hand(Hand::Lead);
        filter_no_head.select_hand(Hand::Rear);
        filter_no_head.select_target(Target::Body);

        let mut filter_no_body = CombinationFilters::new();
        filter_no_body.select_punch(PunchType::JabNCross);
        filter_no_body.select_punch(PunchType::Hooks);
        filter_no_body.select_punch(PunchType::Uppercuts);
        filter_no_body.select_hand(Hand::Lead);
        filter_no_body.select_hand(Hand::Rear);
        filter_no_body.select_target(Target::Head);

        assert_eq!(false, combo.satisfies(&filter_no_head));
        assert_eq!(false, combo.satisfies(&filter_no_body));
    }

    #[test]
    fn given_combo_2_points_and_filter_nothing_when_checking_fi_combo_satisfies_filter_then_true() {
        let combo = Combo::new_two(Punch::jab(), Punch::cross_body());

        let mut filter_nothing = CombinationFilters::new();
        filter_nothing.select_punch(PunchType::JabNCross);
        filter_nothing.select_punch(PunchType::Hooks);
        filter_nothing.select_punch(PunchType::Uppercuts);
        filter_nothing.select_hand(Hand::Lead);
        filter_nothing.select_hand(Hand::Rear);
        filter_nothing.select_target(Target::Body);
        filter_nothing.select_target(Target::Head);

        assert_eq!(true, combo.satisfies(&filter_nothing));
    }

    #[test]
    fn given_hands_filters_when_get_hands_value_the_correct_value_returned() {
        let mut filter_hands = CombinationFilters::new();
        
        filter_hands.select_hand(Hand::Lead);
        filter_hands.select_hand(Hand::Rear);
        assert_eq!(1, filter_hands.get_hand_as_i32());

        filter_hands.remove_hand(Hand::Rear);
        assert_eq!(0, filter_hands.get_hand_as_i32());

        filter_hands.remove_hand(Hand::Lead);
        filter_hands.select_hand(Hand::Rear);
        assert_eq!(2, filter_hands.get_hand_as_i32());

        filter_hands.select_hand_from_i32(1);
        assert_eq!(1, filter_hands.get_hand_as_i32());

        filter_hands.select_hand_from_i32(0);
        assert_eq!(0, filter_hands.get_hand_as_i32());

        filter_hands.select_hand_from_i32(2);
        assert_eq!(2, filter_hands.get_hand_as_i32());
    }
}
