# Boxing training

A simple client side only boxing training web app.

### requirements
Run on rust 1.76. You should have trunk wasmbindgen and trunk installed.

### how to run

```bash
trunk serve
```

### Updating ci build image

 * Increment the base version of rust used
 * build it using docker build -t girouxjep/rust-trunk-wasm-bindgen-cli:YOUR_VERSION -f Dockerfile-ci  ./docker
 * push it
 * update gitlab-ci.yml
